<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Register extends Authenticatable
{
    public $timestamps = false;
    protected $table   = 'admins';
}
