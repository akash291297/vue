<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crud;
use Validator,Auth;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registerList        =  Crud::orderBy('id','desc')->paginate(3);
        $response['return']  = true;
        $response['message'] ='Data Found';
        $response['data']    = $registerList;
         return Response()->json($registerList,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $response=Auth::logout();
        $response['return'] =true;
        return Reponse()->json($ressponse,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= [
            "name"  => "required",
            "email" => "required|email",
        ];

        $errors=[
            "name.required"  => "Name is Required",
            "email.required" => "Email is Required"
        ];

        $validator = Validator::make($request->all(),$rules,$errors);
        if($validator->fails()){
            $keys = array_keys($validator->getMessageBag()->toArray());
            $response['error'] = $validator->getMessageBag()->toArray();
            $response['errors_keys'] = $keys;
            return Response()->json($response,400);
        }
        
        $registerData = new Crud();
        $registerData->name  = $request->name;
        $registerData->email = $request->email;
        if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(300,150)->save(public_path('images/').$name);
         
          $registerData->image = $name;
        }
       // $registerData->image = $request->image;
        $registerData->save();
        $response['return']=true;
        $response['data'] = $registerData;
        if($registerData){
            $registerList =  Crud::orderBy('id','desc')->paginate(3);
            return Response()->json($registerList,200);
        }
        
        
       
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $registerEdit = Crud::find($id);
        if($registerEdit == null){
            $response['return'] = false;
            $response['message'] = 'Data Not Found';
            return Response()->json($response,400);
        }
        $response['return'] = true;
        $response['data']=$registerEdit;
        return Response()->json($registerEdit,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        $rules= [
            "crud_id" => "required|numeric",
            "name"    => "required",
            "email"   => "required|email",
        ];

        $errors=[
            "name.required"  => "Name is Required",
            "email.required" => "Email is Required"
        ];

        $validator = Validator::make($request->all(),$rules,$errors);
        if($validator->fails()){
            $keys = array_keys($validator->getMessageBag()->toArray());
            $response['error'] = $validator->getMessageBag()->toArray();
            $response['errors_keys'] = $keys;
            return Response()->json($response,400);
        }
       
        
        $registerData = Crud::find($request->crud_id);
        $registerData->name  = $request->name;
        $registerData->email = $request->email;
        if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(300,150)->save(public_path('images/').$name);
          $registerData->image = $name;
        }
        // $registerData->image = $request->image;
        $registerData->save();
        $response['return']=true;
        $response['data']  = $registerData;
        if($registerData){
            $registerList =  Crud::orderBy('id','desc')->paginate(3);
            return Response()->json($registerList,200);
        }
        
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
                $registerDelete = Crud::find($id);
                $registerDelete -> delete();
                $response['return']  =  true;
                $response['message'] = 'Record Deleted Successfully';
                $response['deleted data']= $registerDelete;
                if($registerDelete){
                    $registerList =  Crud::orderBy('id','desc')->paginate(5);
                    return Response()->json($registerList,200);
                }
                
    }

      public function verify(Request $request){
        return $request->user()->only('name','email');
    }
}
