<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function register(Request $request){
    	$rules = [
    		'name'     => 'required',
    		'email'    => 'required|email|unique:users,email',
    		'password' => 'required',
            
            
    	];
    	$errors = [
    		'name.required'  => 'Name Field is rquired',
    		'email.required' => 'Email field is required',
    		'email.email'    => 'Enter valid email',
			'email.unique'   => 'Email should be unique',
			'password.required'=>"password is required"
    		
    	];
    	$validator = Validator::make($request->all(), $rules, $errors);
    	
    	if($validator->fails()){
    		$keys                    =  array_keys($validator->getMessageBag()->toArray());
            $response['errors']      =  $validator->getMessageBag()->toArray();
            $response['errors_keys'] =  $keys;
            return Response()->json($response, 400);
    	}

    	$userRegister            =  new User();
    	$userRegister->name      =  $request->name;
    	$userRegister->email     =  $request->email;
    	$userRegister->password  =  bcrypt($request->password);
    	$userRegister->api_token =  Str::random(10);
    	$userRegister->save();
    	$response['data']   = $userRegister;
    	$response['return'] = true;
    	return response()->json($response,200);

    }
}
