<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Register;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function login(Request $request){
    	$rules=[
    		"email"    =>  "required|email",
    		"password" =>   "required"
    	];
    	$errors =[
    		'name.required'  => 'Name Field is rquired',
    		'email.required' => 'Email field is required',
    		'email.email'    => 'Enter valid email', 
    		'email.unique'   => 'Email should be unique',
    	];

    	$validator = Validator::make($request->all(),$rules,$errors);
		
    	if($validator->fails()){
    		$keys = array_keys($validator->getMessageBag()->toArray());
    		$response['error']      = $validator->getMessageBag()->toArray();
    		$response['error_keys'] = $keys;
    		return Response()->json($response,400);
    	}

    	$credentials = $request->only('email','password');

       if(Auth::attempt($credentials)){
		   
		    $response = Auth::user()->api_token;
			return response()->json($response,200);
       }
       else{
		$response['return'] = false;
		$response['errors_keys'] = ['message'];
		$response['error']['message'] = ['Invalid Email or Password.'];
		return response()->json($response,400);
	   }
    } 
}
