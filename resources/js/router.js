import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './components/AppComponent.vue'
import Angular from './components/pages/AngularComponent.vue'
import Bootstrap from './components/pages/BootstrapComponent.vue'
import Cpp from './components/pages/CppComponent.vue'
import Jquery from './components/pages/JqueryComponent.vue'
import Laravel from './components/pages/LaravelComponent.vue'
import Python from './components/pages/PythonComponent.vue'
import Login from './components/LoginComponent.vue'
import Register from './components/RegisterComponent.vue'
import Index from './components/Crud/IndexComponent.vue'
Vue.use(VueRouter)
const routes = [
    
    {
    	path: '/',
    	component: App
    },
    {
    	path: '/angular',
    	component: Angular
    },
    {
    	path: '/bootstrap',
    	component: Bootstrap
    },
    {
    	path: '/cpp',
    	component: Cpp
    },
    {
    	path: '/jquery',
    	component:Jquery
    },
    {
    	path: '/laravel',
    	component:Laravel
    },
    {
    	path:'/python',
    	component:Python
    },
    {
        path: '/login',
        component:Login
    },
    {
        path:'/register',
        component:Register
    },
    {
        path:'/index',
        component:Index,
        beforeEnter: (to, from, next) => {
            if(localStorage.getItem('token')){
                next();
            }else{
                next('/login');
            }
          }
        
    }
]

const router = new VueRouter({routes})
router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token') || null
    window.axios.defaults.headers['Authorization'] = "Bearer " +token;
    next();
})
 
export default router